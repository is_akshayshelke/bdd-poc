@FunctionalTest
Feature: NEST Login
  Login feature of the NEST webapplication

 @RegressionTest
  Scenario Outline: User should login with valid username and password
  	Given NEST login page is loaded
    When User enters "<username>" and "<password>"
    And Click on Login button
    Then User will navigate to home page.

    Examples: 
      | username  | password |
      | yourUsername| yourPassword |
      
  @SmokeTest
  Scenario: Verify header of the 'My Attendace Records' page
 
 		Given NEST login page is loaded
 		And User enters "yourUsername" and "yourPassword" and navigates to NEST Home page
 		When User clicks on Navigation Bar
 		And User clicks on My Records button under Attendace button
 		Then Verify user is navigated to 'My Attendance Records' page 