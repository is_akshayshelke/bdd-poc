package com.bdd.poc.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NEST_HomePage {

	WebDriver driver;

	@FindBy(xpath = "//span[@title='Navigation']")
	WebElement buttonNavigationBar;
	@FindBy(xpath = "//span[text()='Attendance']")
	WebElement buttonAttendance;
	@FindBy(xpath = "//a[text()='My Records']")
	WebElement linkMyRecords;

	String urlNEST_HomePage = "https://nest.infostretch.com/#/myview";

	public NEST_HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public String getNEST_HomePageURL() {
		return urlNEST_HomePage;
	}

	public void clickButtonNavigationBar() {
		buttonNavigationBar.click();
	}

	public void clickLinkMyRecords() throws Throwable {
		buttonAttendance.click();
		Thread.sleep(5000);
		linkMyRecords.click();
		Thread.sleep(10000);
	}

}
