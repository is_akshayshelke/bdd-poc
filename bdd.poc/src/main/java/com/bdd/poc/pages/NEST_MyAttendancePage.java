package com.bdd.poc.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NEST_MyAttendancePage {

	WebDriver driver;

	@FindBy(xpath = "//div[text()='My Attendance Records']")
	WebElement headerMyAttendanceRecord;
	
	public NEST_MyAttendancePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public String getHeaderMyAttendanceRecordText() {
		return headerMyAttendanceRecord.getText();
	}
}
