package com.bdd.poc.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NEST_LoginPage {

	WebDriver driver;

	@FindBy(xpath = "//input[@id='username']")
	WebElement textBoxUsername;
	@FindBy(xpath = "//input[@id='password']")
	WebElement textBoxPassword;
	@FindBy(xpath = "//button[@type='submit']")
	WebElement buttonLogin;

	String urlNEST = "https://nest.infostretch.com/";

	public NEST_LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void invokeLoginPage() {
		driver.get(urlNEST);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void enterUsername(String username) {

		textBoxUsername.sendKeys(username);
	}

	public void enterPassword(String password) {

		textBoxPassword.sendKeys(password);
	}

	public void clickLogin() {

		buttonLogin.click();
	}

	public void loginWithValidUser(String username, String password) throws Throwable {
		textBoxUsername.sendKeys(username);
		textBoxPassword.sendKeys(password);
		buttonLogin.click();
		Thread.sleep(10000);
	}
}
