package com.bdd.poc.steps;

import org.openqa.selenium.WebDriver;

import com.bdd.poc.pages.NEST_HomePage;
import com.bdd.poc.pages.NEST_LoginPage;
import com.bdd.poc.pages.NEST_MyAttendancePage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.messages.internal.com.google.common.base.Verify;

public class StepDefination {
	
	WebDriver driver = Hooks.driver;
	
	NEST_HomePage homePage;
	NEST_LoginPage loginPage;
	NEST_MyAttendancePage myAttendancePage;

	@Given("NEST login page is loaded")
	public void invokeNEST_LoginPage() throws Throwable {

		loginPage = new NEST_LoginPage(driver);
		loginPage.invokeLoginPage();
	}

	@When("User enters {string} and {string}")
	public void enterCredentials(String username, String password) throws Throwable {
		loginPage.enterUsername(username);
		loginPage.enterPassword(password);
	}

	@When("^Click on Login button$")
	public void clickButtonLogin() throws Throwable {
		loginPage.clickLogin();
		Thread.sleep(10000);
	}

	@Then("^User will navigate to home page\\.$")
	public void verifyUserIsOnHomePage() throws Throwable {
		homePage = new NEST_HomePage(driver);
		Verify.verify(homePage.getNEST_HomePageURL().equals(driver.getCurrentUrl()));
	}

	@Given("User enters {string} and {string} and navigates to NEST Home page")
	public void loginAndNavigateToHomePage(String username, String password) throws Throwable{
		
		loginPage = new NEST_LoginPage(driver);
		loginPage.loginWithValidUser(username, password);
		
		homePage = new NEST_HomePage(driver);	
		if(homePage.getNEST_HomePageURL().equals(driver.getCurrentUrl()))
			System.out.println("User is on NEST home page");
	}

	@When("^User clicks on Navigation Bar$")
	public void clickOnNavigationBar() throws Throwable {
		homePage.clickButtonNavigationBar();
	}
	
	@When("^User clicks on My Records button under Attendace button$")
	public void clickOnMyRecords() throws Throwable {
		homePage.clickLinkMyRecords();
	}

	@Then("^Verify user is navigated to 'My Attendance Records' page$")
	public void verifyUserIsOnMyAttendancePage() throws Throwable {
		
		myAttendancePage = new NEST_MyAttendancePage(driver);
		Verify.verify(myAttendancePage.getHeaderMyAttendanceRecordText().equals("My Attendance Records"));
	}

}
