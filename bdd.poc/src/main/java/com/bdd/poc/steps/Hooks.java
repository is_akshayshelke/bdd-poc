package com.bdd.poc.steps;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;

public class Hooks {

	static WebDriver driver;

	@Before
	public void beforeScenario() {
		System.setProperty("webdriver.chrome.driver", "server\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@After
	public void afterScenario() {
		driver.close();
	}
}