package com.bdd.poc.runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		strict = true,
		features = "features",
		glue="com.bdd.poc.steps",
		//format = {"pretty", "html:target/reports"}
		tags = {"@FunctionalTest"},
		monochrome = true,
		plugin = { "pretty", "html:target/cucumber-reports" }
 )

public class JUnitRunner {
	
}
